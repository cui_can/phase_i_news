package com.nx.arch.transactionmsg.mybatis;

import java.sql.Connection;
import java.util.List;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;

import com.nx.arch.transactionmsg.TransactionMsgClient;
import com.nx.arch.transactionmsg.model.TxMsgDataSource;
import com.nx.arch.transactionmsg.utils.Config;

public class MybatisTransactionMsgClient extends TransactionMsgClient {
    
    private SqlSessionTemplate sessionTemplate;
    
    private SqlSessionFactory sqlSessionFactory;
    
    public MybatisTransactionMsgClient(SqlSessionTemplate sessionTemplate, String mqAddr, List<TxMsgDataSource> dbDataSources, List<String> topicLists) {
        super(mqAddr, dbDataSources, topicLists, new Config());
        this.sessionTemplate = sessionTemplate;
    }
    
    public MybatisTransactionMsgClient(SqlSessionFactory sqlSessionFactory, String mqAddr, List<TxMsgDataSource> dbDataSources, List<String> topicLists, Config config) {
        super(mqAddr, dbDataSources, topicLists, config);
        this.sqlSessionFactory = sqlSessionFactory;
        try {
            this.sessionTemplate = new SqlSessionTemplate(sqlSessionFactory);
        } catch (Exception e) {
            // Auto-generated catch block
            LOGGER.error("get sqlSessionFactory fail", e);
        }
    }
    
    public MybatisTransactionMsgClient(SqlSessionFactory sqlSessionFactory, String mqAddr, List<TxMsgDataSource> dbDataSources, List<String> topicLists) {
        this(sqlSessionFactory, mqAddr, dbDataSources, topicLists, new Config());
    }

    @Override
    public Long sendMsg(String content, String topic, String tag)
        throws Exception {
        // Auto-generated method stub
        Long id = null;
        try {
            Connection con = sessionTemplate.getConnection();
            id = super.sendMsg(con, content, topic, tag, 0);
            return id;
        } catch (Exception ex) {
            // Auto-generated catch block
            LOGGER.error("sendMsg fail topic {} tag {} ", topic, tag, ex);
            throw new RuntimeException(ex);
        }
    }
    
    public Long sendMsg(String content, String topic, String tag, int delay)
        throws Exception {
        // Auto-generated method stub
        Long id = null;
        try {
            Connection con = sessionTemplate.getConnection();
            id = super.sendMsg(con, content, topic, tag, delay);
            return id;
        } catch (Exception ex) {
            // Auto-generated catch block
            LOGGER.error("sendMsg fail topic {} tag {} delay {}", topic, tag, delay, ex);
            throw new RuntimeException(ex);
        }
    }
    
    public SqlSessionTemplate getSessionTemplate() {
        return sessionTemplate;
    }
    
    public void setSessionTemplate(SqlSessionTemplate sessionTemplate) {
        this.sessionTemplate = sessionTemplate;
    }
    
    public SqlSessionFactory getSqlSessionFactory() {
        return sqlSessionFactory;
    }
    
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
        this.sqlSessionFactory = sqlSessionFactory;
    }
    
    public void setHistoryMsgStoreTime(int historyMsgStoreTime) {
        this.getConfig().setHistoryMsgStoreTime(historyMsgStoreTime);
    }
    
    public int getHistoryMsgStoreTime() {
        return this.getConfig().getHistoryMsgStoreTime();
    }
    
}
