package com.nx.arch.transactionmsg.model;

import java.util.Date;

/**
 * @类名称 MsgInfo.java
 * @类描述 事务消息实体
 * @作者  庄梦蝶殇 linhuaichuan@naixuejiaoyu.com
 * @创建时间 2020年4月12日 下午4:35:24
 * @版本 1.0.0
 *
 * @修改记录
 * <pre>
 *     版本                       修改人 		修改日期 		 修改内容描述
 *     ----------------------------------------------
 *     1.0.0 		庄梦蝶殇 	2020年4月12日             
 *     ----------------------------------------------
 * </pre>
 */
public class MsgInfo {
    /**
     * 主键
     */
    private Long id;
    
    /**
     * 事务消息
     */
    private String content;
    
    /**
     * 主题
     */
    private String topic;
    
    /**
     * 标签
     */
    private String tag;
    
    /**
     * 状态：1-等待，2-发送
     */
    private int status;
    
    /**
     * 创建时间
     */
    private Date createTime;
    
    /**
     * 延迟时间(单位：s)
     */
    private int delay;
    
    public MsgInfo() {
        
    }
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getContent() {
        return content;
    }
    
    public void setContent(String content) {
        this.content = content;
    }
    
    public String getTopic() {
        return topic;
    }
    
    public void setTopic(String topic) {
        this.topic = topic;
    }
    
    public String getTag() {
        return tag;
    }
    
    public void setTag(String tag) {
        this.tag = tag;
    }
    
    public int getStatus() {
        return status;
    }
    
    public void setStatus(int status) {
        this.status = status;
    }
    
    public Date getCreateTime() {
        return createTime;
    }
    
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    
    public void setDelay(int delay) {
        this.delay = delay;
    }
    
    public int getDelay() {
        return this.delay;
    }
    
    @Override
    public String toString() {
        return "MsgInfo [id=" + id + ", content=" + content + ", topic=" + topic + ", tag=" + tag + ", status=" + status + ", createTime=" + createTime + ", delay=" + delay + "]";
    }
    
}
